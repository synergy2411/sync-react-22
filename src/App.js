import React, { Suspense, useEffect, useState } from 'react';
import { Route, Routes, useLocation, useNavigate } from 'react-router-dom';
import BounceLoader from 'react-spinners/BounceLoader';

import './App.css';
// import Expenses from './Components/Expenses/Expenses';
import Header from './Components/Header/Header';
import ClassBased from './Components/playground/ClassBased/ClassBased';
import Login from './Components/playground/FormDemo/Login';
import UseEffectDemoHook from './Components/playground/HooksDemo/UseEffectDemoHook';
import TreeViewComp from './Components/TreeViewComp/TreeViewComp';
import AuthContext from './context/auth-context';
import AddBook from './pages/Authors/AddBook/AddBook';
import AuthorInfo from './pages/Authors/AuthorInfo/AuthorInfo';
import Authors from './pages/Authors/Authors';
import Counter from './pages/Counter/Counter';
import AddNew from './pages/Courses/AddNew/AddNew';
import CourseItem from './pages/Courses/CourseItem/CourseItem';
import Courses from './pages/Courses/Courses';
import NotFound from './pages/NotFound/NotFound';


const Expenses = React.lazy(() => import("./Components/Expenses/Expenses"))

function App() {

  const navigate = useNavigate()
  const location = useLocation()
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const logoutHandler = () => {
    localStorage.clear();
    setIsLoggedIn(false);
    navigate("/login")
  }
  useEffect(() => {
    if (location.pathname === '/') {
      navigate("/login")
    }
  }, [])
  return (
    // <TreeViewComp />
    <AuthContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
      <Suspense fallback={<BounceLoader size="70px" color='#ff45ff' />}>
        <div className='container'>
          <div className='row'>
            <div className='col-9'>
              <Header />
            </div>
            {isLoggedIn && <div className='col-3'>
              <button className='btn btn-block btn-outline-danger'
                onClick={logoutHandler}>Logout</button>
            </div>}
          </div>
          <br />
          <Routes>
            <Route path='/login' element={<Login />} />
            <Route path="/counter" element={<Counter />} />
            <Route path='/expenses' element={<Expenses />} />
            <Route path="/use-effect" element={<UseEffectDemoHook />} />
            <Route path="/class-based" element={<ClassBased />} />
            <Route path='/courses/*' element={<Courses />} >
              <Route path=":courseName/:courseDuration" element={<CourseItem />} />
              <Route path="add/new" element={<AddNew />} />
            </Route>
            <Route path='/authors/*' element={<Authors />}>
              <Route path=':authorId/:authorName' element={<AuthorInfo />} />
              <Route path=":authorId/add-new" element={<AddBook />} />
            </Route>
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
      </Suspense>
    </AuthContext.Provider>
  );
}

export default App;

// http://localhost:3000/courses/angular/80




// http://localhost:3000/

// JSX -> React "Elements" -> JavaScript Object -> Virtual DOM -> Real DOM

// const [isLoggedIn, setIsLoggedIn] = useState(false);

{/* <AuthContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
<div className='container'>

  <ParentComp />

  {/* <UseReducerHookDemo /> */}

{/* <UseContextHookDemo /> */ }

{/* <UseEffectDemoHook /> */ }
{/* <br />
<Login />
<ErrorBoundary>
  <ClassBased />
</ErrorBoundary>
<hr />
  <ErrorBoundary>
    <Expenses />
  </ErrorBoundary>
</div>
</AuthContext.Provider> */}