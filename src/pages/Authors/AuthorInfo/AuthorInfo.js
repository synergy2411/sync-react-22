import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

const AuthorInfo = () => {

    const { authorId, authorName } = useParams();
    const [books, setBooks] = useState([]);
    const navigate = useNavigate();

    const fetchBooks = async () => {
        try {
            const response = await axios.get(`http://localhost:9000/books?authorId=${authorId}`)
            setBooks(response.data)
        } catch (err) {
            console.log(err);
        }
    }

    useEffect(() => {
        fetchBooks()
    }, [authorId])


    const addBookHandler = () => {
        navigate(`/authors/${authorId}/add-new`)
    }

    const bookDeleteHandler = async (bookId) => {
        try {
            await axios.delete(`http://localhost:9000/books/${bookId}`)
            fetchBooks()
        } catch (err) {
            console.log(err)
        }
    }

    if (books.length === 0) {
        return (
            <>
                <h5>Be the first to add book</h5>
                <button className='btn btn-dark'
                    onClick={addBookHandler}>Add Book</button>
            </>)
    }

    return (
        <div className='row'>
            <div className='col-8 offset-2'>
                <div className='text-center'>
                    <div className='row'>
                        <div className='col-6 offset-3'>
                            <button className='btn btn-dark'
                                onClick={addBookHandler}>Add Book</button>
                        </div>
                    </div>
                    <h3>Showing the result for {authorName.toUpperCase()} </h3>
                    <ul className='list-group'>
                        {books.map(book => <li key={book.id}
                            className="list-group-item">
                            {book.title.toUpperCase()} - {book.numOfPages} Pages
                            <span className='float-right'>
                                <button onClick={() => bookDeleteHandler(book.id)} className='btn btn-outline-danger btn-sm'>×</button>
                            </span>
                        </li>)}
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default AuthorInfo;
