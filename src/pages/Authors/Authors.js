import axios from 'axios';
import React, { Fragment, useEffect, useState } from 'react';
import BounceLoader from 'react-spinners/BounceLoader';

import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import Author from './Author/Author';

const sortByOrder = (isAscending, authors) => {
    if (isAscending) {
        return authors.sort((a, b) => {
            if (a.name > b.name) {
                return 1
            } else if (a.name < b.name) {
                return -1
            } else {
                return 0
            }
        })
    } else {
        return authors.sort((a, b) => {
            if (a.name > b.name) {
                return -1
            } else if (a.name < b.name) {
                return 1
            } else {
                return 0
            }
        })
    }
}

const Authors = () => {

    const [authors, setAuthors] = useState([]);

    const navigate = useNavigate();
    const location = useLocation();

    const query = new URLSearchParams(location.search);
    const isAscending = query.get("sort") === "asc";

    useEffect(() => {
        if (!localStorage.getItem("user")) {
            return navigate("/login")
        }
    }, [navigate])

    useEffect(() => {
        const fetchAuthors = async () => {
            try {
                const response = await axios.get("http://localhost:9000/authors")
                setTimeout(() => {
                    setAuthors(response.data)
                }, 1000)
            } catch (err) {
                console.log(err);
            }
        }
        fetchAuthors()
    }, [])


    const sortHandler = () => {
        navigate(`/authors?sort=${isAscending ? 'desc' : 'asc'}`)
    }

    const sortedAuthors = sortByOrder(isAscending, authors);

    if (authors.length === 0) {
        return <BounceLoader size="80px" color='#ff45ff' />
    }

    return (
        <Fragment>
            <div className='row'>
                <div className='col-4 offset-4'>
                    <button onClick={sortHandler} className="btn btn-info btn-block">
                        {isAscending ? 'Descending' : 'Acsending'}
                    </button>
                </div>
            </div>
            <br />
            <div className='row'>
                {sortedAuthors.map(author => <Author
                    key={author.id}
                    name={author.name}
                    id={author.id}
                    email={author.email} />)}
                <hr />
            </div>
            <br />
            <br />
            <Outlet />
        </Fragment>
    );
}

export default Authors;
