import React from 'react';
import { useNavigate } from 'react-router-dom';

const Author = (props) => {
    const navigate = useNavigate()

    const moreInfoHandler = () => {
        navigate(`/authors/${props.id}/${props.name}`)
    }
    return (
        <div className='col-md-3 col-sm-4'>
            <div className='card'>
                <div className='card-body text-center'>
                    <h6>{props.name.toUpperCase()}</h6>
                    <p>{props.email}</p>
                    <button className='btn btn-light btn-block'
                        onClick={moreInfoHandler}>More Info</button>
                </div>
            </div>
        </div>
    );
}

export default Author;
