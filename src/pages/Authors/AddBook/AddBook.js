import axios from 'axios';
import ReactDOM from 'react-dom';

import React, { Fragment, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import classes from './AddBook.module.css';

const AddBook = () => {
    const { authorId } = useParams()
    const navigate = useNavigate()

    const [title, setTitle] = useState('')
    const [numOfPages, setNumOfPages] = useState('')

    const submitHandler = async (event) => {
        event.preventDefault();
        try {
            await axios.post("http://localhost:9000/books", {
                id: Date.now().toString(),
                title,
                numOfPages: Number(numOfPages),
                authorId
            })
            navigate(`/authors`)
        } catch (err) {
            console.log(err);
        }
    }

    return ReactDOM.createPortal(
        <Fragment>
            <div className={classes["backdrop"]}>
                <div className={classes["alert-box"]}>
                    <div className='card'>
                        <div className='card-header'>
                            <h6 className='text-center'>Add New Book</h6>
                        </div>
                        <div className="card-body">
                            <form onSubmit={submitHandler}>
                                {/* title */}
                                <div className='form-group'>
                                    <label htmlFor='title'>Title : </label>
                                    <input type="text" id='title'
                                        className='form-control'
                                        value={title}
                                        onChange={(event) => setTitle(event.target.value)} />
                                </div>
                                {/* numOfPages */}
                                <div className='form-group'>
                                    <label htmlFor='numOfPages'>Number Of Pages : </label>
                                    <input type="number" id='numOfPages'
                                        className='form-control'
                                        value={numOfPages}
                                        onChange={(event) => setNumOfPages(event.target.value)}
                                    />
                                </div>
                                <br />
                                {/* button */}
                                <div className='form-group'>
                                    <button type='submit' className='btn btn-dark btn-block'>
                                        Add Book
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
        , document.querySelector("#portal-container"));
}

export default AddBook;
