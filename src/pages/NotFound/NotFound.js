import React from 'react';
import { useNavigate, Navigate } from 'react-router-dom';

const NotFound = () => {

    const navigate = useNavigate()

    const gotoHome = () => {
        navigate("/")
    }

    // if(user.isAuthenticated()){
    //     <Navigate to="/dashboard" />
    // }else{
    //     <Navigate to="/login" />
    // }

    return (
        <div>
            <h3>Page NOT found.</h3>

            <button className='btn btn-dark' onClick={gotoHome}>Go to Home Page</button>
        </div>
    );
}

export default NotFound;
