import React from 'react';
import { useParams } from 'react-router-dom';

const CourseItem = () => {
    const params = useParams()
    return (
        <div>
            <h5>Course Name : {params.courseName}</h5>
            <h6>Course Duration : {params.courseDuration}</h6>
        </div>
    );
}

export default CourseItem;
