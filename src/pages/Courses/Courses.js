import React from 'react';
import { Routes, Route, Link, Outlet } from 'react-router-dom';
import CourseItem from './CourseItem/CourseItem';

const Courses = () => {
    return (
        <div>
            <h4>All Courses</h4>
            <Link to="/courses/angular/80">Angular</Link> ||
            <Link to="/courses/react/60">React</Link>

            <br />

            <Outlet />
            {/* <Routes>
                <Route path=":courseName/:courseDuration" element={<CourseItem />} />
            </Routes> */}
        </div>
    );
}

export default Courses;


// http://localhost:3000/courses/vue