import React from 'react';
import { useDispatch, connect } from 'react-redux';
import * as fromActions from '../../../store/actions/rootActions';


const CounterButtons = (props) => {
    const counterDispatch = useDispatch();

    return (
        <div>
            <button className='btn btn-primary btn-sm'
                onClick={() => counterDispatch({ type: fromActions.INCREMENT })}>Increase</button>
            <button className='btn btn-warning btn-sm'
                onClick={() => counterDispatch({ type: fromActions.DECREMENT })}>Decrease</button>
            <button className='btn btn-success btn-sm'
                onClick={() => counterDispatch({ type: fromActions.ADD_COUNTER, value: 10 })}>Add (10)</button>
            <button className='btn btn-dark btn-sm'
                onClick={() => counterDispatch(fromActions.asyncSubtractCounter(2))}>Subtract (2)</button>
            {/* <button className='btn btn-dark btn-sm'
                onClick={() => counterDispatch(fromActions.onSubtractCounter(5))}>Subtract (5)</button> */}
            {/* <button className='btn btn-primary btn-sm'
                onClick={() => props.onIncrease()}>Increase</button> */}
        </div>
    );
}

export default CounterButtons;

// const mapDispatchToProps = dispatch => {
//     return {
//         onIncrease: () => dispatch({ type: "INCREMENT" })
//     }
// }

// const connected = connect(null, mapDispatchToProps)

// export default connected(CounterButtons);
