import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as fromActions from '../../../store/actions/rootActions';

const CounterResult = () => {

    const result = useSelector(store => store.res.result)
    const counter = useSelector(store => store.ctr.counter)
    const resultDispatch = useDispatch();

    return (
        <div>
            <button className='btn btn-lg btn-block'
                onClick={() => resultDispatch({ type: fromActions.STORE_RESULT, value: counter })}>Store Result</button>
            <br />
            <ul className='list-group'>
                {result.map((r, i) => <li onClick={() => resultDispatch(fromActions.onDeleteResult(i))}
                    className='list-group-item' key={i}>{r}</li>)}
            </ul>
        </div>
    );
}

export default CounterResult;
