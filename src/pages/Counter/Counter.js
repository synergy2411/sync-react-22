import React from 'react';
import { useSelector, connect } from 'react-redux';
import CounterButtons from './CounterButtons/CounterButtons';
import CounterResult from './CounterResult/CounterResult';

const Counter = (props) => {

    // console.log("PROPS : ", props);
    const counter = useSelector(store => {
        // console.log("STORE : ", store);
        return store.ctr.counter
    })

    return (
        <div className='row'>
            <div className='col-6 offset-3'>
                <p className='display-4'>Counter : {counter}</p>
                <br />
                <CounterButtons />
                <hr />
                <CounterResult />
            </div>
        </div>
    );
}

export default Counter;

// const mapStateToProps = (state) => {
//     return {
//         counter: state.counter
//     }
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//         onIncrease: () => dispatch({ type: "INCREMENT" })
//     }
// }

// const connected = connect(mapStateToProps, mapDispatchToProps)

// export default connected(Counter);
