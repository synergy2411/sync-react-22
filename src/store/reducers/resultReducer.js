import * as fromActions from '../actions/rootActions';

const initialState = {
    result: []
}

export default function resultReducer(state = initialState, action) {
    switch (action.type) {
        case fromActions.STORE_RESULT: {
            return {
                ...state,
                result: [action.value, ...state.result]
            }
        }
        case fromActions.DELETE_RESULT: {
            return {
                ...state,
                result: state.result.filter((r, i) => i !== action.value)
            }
        }
        default:
            return state;
    }
}