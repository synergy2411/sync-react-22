import resultReducer from "./resultReducer";
import * as fromActions from '../actions/rootActions';

test("should delete the result element from the array", () => {
    const state = resultReducer({ result: [2, 3, 4, 5, 6] }, { type: fromActions.DELETE_RESULT, value: 2 })

    expect(state.result).not.toContain(4)
})


test("should store the counter value in result array", () => {

    const state = resultReducer({ result: [] }, { type: fromActions.STORE_RESULT, value: 10 });

    expect(state.result[0]).toBe(10)

})