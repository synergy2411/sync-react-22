import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from '@redux-devtools/extension';

import Logger from './middlewares/logger';
import counterReducer from './reducers/counterReducer';
import resultReducer from './reducers/resultReducer';


const store = createStore(combineReducers({
    ctr: counterReducer,
    res: resultReducer
}), composeWithDevTools(applyMiddleware(Logger, thunk)))

export default store;

