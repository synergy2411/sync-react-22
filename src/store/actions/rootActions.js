export const INCREMENT = "[COUNTER] to increase the value by one";
export const DECREMENT = "[COUNTER] to Ddecrease the value by one";
export const ADD_COUNTER = "[COUNTER] to add the supplied value";
export const SUBTRACT_COUNTER = "[COUNTER] to subtract the counter by supplied value";
export const STORE_RESULT = "[RESULT] to store current value of counter";
export const DELETE_RESULT = "[RESULT] to delete one value from result";

// Action Creators
export const onDeleteResult = idex => {
    return {
        type: DELETE_RESULT,
        value: idex
    }
}


export const onSubtractCounter = value => {
    // Can write more code here
    // value++;
    return {
        type: SUBTRACT_COUNTER,
        value
    }
}

// Asynchronous Action

export const asyncSubtractCounter = (value) => {
    return (dispatch, getState) => {
        // More Async Code can write here
        setTimeout(() => {
            dispatch(onSubtractCounter(value))
        }, 3000)
    }
}