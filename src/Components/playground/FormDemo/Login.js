import { useContext, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../../../context/auth-context';

const Login = () => {


    const inputUsernameRef = useRef();          // React.createRef() - for class based component
    const [enteredPassword, setEnteredPassword] = useState('')
    const [passwordIsBlurred, setPasswordIsBlurred] = useState(false);

    const ctx = useContext(AuthContext)
    const navigate = useNavigate()

    const submitHandler = event => {
        event.preventDefault();
        console.log("Username : ", inputUsernameRef.current.value)
        console.log("Password : ", enteredPassword);
        if (inputUsernameRef.current.value === "foo@test" && enteredPassword === "foo123") {
            localStorage.setItem("user", JSON.stringify({ username: inputUsernameRef.current.value }))
            ctx.setIsLoggedIn(true)
            navigate(`/authors`)
        }
    }

    const passwordChangeHandler = event => setEnteredPassword(event.target.value)

    const passwordIsValid = enteredPassword.trim() !== '';
    const passwordIsInvalid = passwordIsBlurred && !passwordIsValid

    const formValidity = !passwordIsBlurred && !passwordIsValid;

    return (
        <form onSubmit={submitHandler}>
            <h4>Login Form</h4>
            {/* username */}
            <div className="form-group">
                <label htmlFor="username">Username</label>
                <input type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    ref={inputUsernameRef} />
            </div>
            {/* password */}
            <div className="form-group">
                <label htmlFor="password">Password</label>
                <input type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    value={enteredPassword}
                    onChange={passwordChangeHandler}
                    onBlur={() => setPasswordIsBlurred(true)} />
                {/* error message */}
                {passwordIsInvalid && <p className='alert alert-danger'>Password is mandatory field</p>}
            </div>
            {/* button */}
            <div className="form-group">
                <button className="btn btn-primary"
                    type='submit'
                    disabled={formValidity}>Login</button>
            </div>
        </form>
    )

}

export default Login;