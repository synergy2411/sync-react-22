import { useReducer } from "react";

const initialState = {
    counter: 0,
    result: []
}

const reducerFn = (currState, action) => {
    if (action.type === "INCREMENT") {
        return {
            ...currState,
            counter: currState.counter + 1
        }
    } else if (action.type === "DECREMENT") {
        return {
            ...currState,
            counter: currState.counter - 1
        }
    } else if (action.type === "ADD_COUNTER") {
        return {
            ...currState,
            counter: currState.counter + action.value
        }
    } else if (action.type === "STORE_RESULT") {
        return {
            ...currState,
            result: [currState.counter, ...currState.result]
        }
    } else if (action.type === "DELETE_RESULT") {
        const filteredResult = currState.result.filter((r, i) => i !== action.index)
        return {
            ...currState,
            result: filteredResult
        }
    }
    return currState;
}

const UseReducerHookDemo = () => {

    const [state, dispatch] = useReducer(reducerFn, initialState);

    return (
        <div>
            <h3>Counter : {state.counter}</h3>
            <br />

            <button className="btn btn-primary"
                onClick={() => dispatch({ type: "INCREMENT" })}>Increase</button>
            <button className="btn btn-success"
                onClick={() => dispatch({ type: "DECREMENT" })}>Decrease</button>
            <button className="btn btn-dark"
                onClick={() => dispatch({ type: "ADD_COUNTER", value: 10 })}>ADD</button>
            <hr />
            <button className="btn btn-light btn-lg"
                onClick={() => dispatch({ type: "STORE_RESULT" })}>Store Result</button>
            <br />
            <ul className="list-group">
                {state.result.map((r, i) => <li
                    onClick={() => dispatch({ type: "DELETE_RESULT", index: i })}
                    className="list-group-item" key={i}>{r}</li>)}
            </ul>
        </div>
    )


}

export default UseReducerHookDemo;
