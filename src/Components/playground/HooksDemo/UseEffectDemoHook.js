import { useState, useEffect } from 'react';

const UseEffectDemoHook = () => {

    const [toggle, setToggle] = useState(true)
    const [show, setShow] = useState(true)
    const [searchTerm, setSerachTerm] = useState('')
    const [repos, setRepos] = useState([]);

    useEffect(() => {
        let timer = null;
        if (searchTerm.trim() !== '') {
            timer = setTimeout(() => {
                fetch(`https://api.github.com/users/${searchTerm}/repos`)
                    .then(response => response.json())
                    .then(result => setRepos(result))
                    .catch(console.log)
            }, 1000)
        }
        return () => {
            clearTimeout(timer);
        };
    }, [searchTerm]);           // de

    const searchTermHandler = event => {
        setSerachTerm(event.target.value)
    }

    // useEffect(() => {
    //     console.log("USE EFFECT CALLED");
    //     return () => {
    //         console.log("CLEAN UP");
    //     }
    // }, [toggle])

    const onToggle = () => setToggle(!toggle);
    const onShow = () => setShow(!show);

    return (
        <>
            <button onClick={onToggle}>Toggle</button>
            <button onClick={onShow}>Show</button>

            {toggle && <p>Now you see me - TOGGLE</p>}
            {show && <p>Now you see me - SHOW</p>}

            <br />
            <input type="text" value={searchTerm} onChange={searchTermHandler} />
            <ul>
                {repos.map(repo => <li key={repo.id}>{repo.name}</li>)}
            </ul>
        </>
    )

}

export default UseEffectDemoHook;