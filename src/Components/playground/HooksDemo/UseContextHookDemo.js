import { useContext } from "react";
import AuthContext from "../../../context/auth-context";

const UseContextHookDemo = () => {

    const context = useContext(AuthContext);

    return (
        <div>
            <button onClick={() => context.setIsLoggedIn(!context.isLoggedIn)}>Change Status</button>
            <h4>User is {context.isLoggedIn ? '' : 'NOT'} Logged In!</h4>
        </div>
    )
}

export default UseContextHookDemo;