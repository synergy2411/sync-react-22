import React from 'react';

const ChildComp = (props) => {
    console.log("CHILD COMP");
    return (
        <div>
            {props.toggle && <p>Child Component Output</p>}
        </div>
    );
}

export default React.memo(ChildComp);

// Memoization -> to store
// prevProps.toggle === props.toggle -> does not re-render the component


// Memo() -> does not check for referential Equality
// prevProps.theDemoFn === props.theDemoFn => false
