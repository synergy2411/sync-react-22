import React, { useState, useCallback, useMemo } from 'react';
import ChildComp from './ChildComp';

const ParentComp = () => {
    const [toggle, setToggle] = useState(false)

    console.log("PARENT COMP");

    // const theDemoFn = useCallback(() => {
    //     console.log("The DEMO Function")
    // }, [])

    const theDemoFn = useMemo(() => () => console.log("The DEMO function"), [])

    const numbers = useMemo(() => [1, 2, 3, 4, 5], []);            // [] === [] = false

    return (
        <div>
            <button className='btn btn-dark' onClick={() => setToggle(!toggle)}>Toggle</button>
            {toggle && <p>Parent Component Output</p>}
            <ChildComp toggle={true} theDemoFn={theDemoFn} numbers={numbers} />
        </div>
    );
}

export default ParentComp;


// shouldComponentUpdate(nextState, nextProps){ return nextProps.value === currProps.value }