import React from 'react';
class ClassBased extends React.Component {

    constructor(props) {
        super(props);
        console.log("CONSTRUCTOR");
        this.state = {
            counter: 0,
            posts: []
        }
    }

    increaseCounter() {
        // this.state.counter++             // NEVER EVER MODIFY STATE LIKE THIS - 
        this.setState({
            counter: this.state.counter + 1
        })
    }

    decreaseCounter = () => this.setState({ counter: this.state.counter - 1 })

    componentDidMount() {
        console.log("COMP DID MOUNT");
        // fetch("https://jsonplaceholder.typicode.com/posts")
        //     .then(response => response.json())
        //     .then(result => {
        //         this.setState({ posts: result })
        //     })
        //     .catch(console.log)
    }
    componentDidUpdate() {
        console.log("COMP DID UPDATE");
    }
    componentWillUnmount() {
        console.log("COMP WILL UNMOUNT");
    }
    shouldComponentUpdate() {
        console.log("SHOULD COMP UPDATE");
        // return this.state.counter < 5;
        return true;
    }

    render() {
        console.log("RENDER");
        if (this.state.counter === 5) {
            throw new Error("Greater than five ERROR")
        } else {
            return (
                <div className='container'>
                    <h4>Counter : {this.state.counter}</h4>
                    <button className='btn btn-primary' onClick={this.increaseCounter.bind(this)}>+</button>
                    <button className='btn btn-primary' onClick={this.decreaseCounter}>-</button>
                    <hr />
                    <ul>
                        {this.state.posts.map(post => <li>{post.title}</li>)}
                    </ul>
                </div>
            )
        }
    }
}

export default ClassBased;