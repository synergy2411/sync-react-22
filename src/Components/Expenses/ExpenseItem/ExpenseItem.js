import ExpenseDate from "../ExpenseDate/ExpenseDate";

const ExpenseItem = (props) => {

    const deleteClickHandler = () => props.deleteExpense(props.id)
    return (
        <div className="col-4">
            <div className="card">
                <div className="card-header">
                    <h6 className="text-center">{props.title.toUpperCase()}</h6>
                </div>
                <div className="card-body">
                    <p className="lead">Amount : ${props.amount}</p>
                    <ExpenseDate createdAt={props.createdAt} />
                    <button className="btn btn-outline-danger btn-block"
                        onClick={deleteClickHandler}>Delete</button>
                </div>
            </div>
        </div>
    )

}

export default ExpenseItem;