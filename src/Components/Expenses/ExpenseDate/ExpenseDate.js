import { useContext } from "react";
import AuthContext from "../../../context/auth-context";

const ExpenseDate = (props) => {

    const context = useContext(AuthContext)
    const day = props.createdAt.toLocaleString("en-US", { day: "numeric" })
    const month = props.createdAt.toLocaleString("en-US", { month: "long" })
    const year = props.createdAt.getFullYear();

    return (
        <>
            {context.isLoggedIn && <p>Created At : {month} {day}, {year}</p>}
        </>
    )

    // return (
    //     <AuthContext.Consumer>
    //         {
    //             (context) => {
    //                 return (
    //                     <>
    //                         {context.isLoggedIn && <p>User is Logged in!!</p>}
    //                         <p>Context Working here</p>
    //                     </>
    //                 )
    //             }
    //         }
    //     </AuthContext.Consumer>
    // )
}

export default ExpenseDate;