import { useState, useContext } from 'react';
import AuthContext from '../../context/auth-context';
import AddExpense from './AddExpense/AddExpense';
import ExpenseFilter from './ExpenseFilter/ExpenseFilter';
import ExpenseItem from "./ExpenseItem/ExpenseItem";


const INITIAL_EXPENSES = [
    { id: "e001", title: "grocery", amount: 12.99, createdAt: new Date("Dec 20, 2020") },
    { id: "e002", title: "shopping", amount: 19.99, createdAt: new Date("Aug 2, 2021") },
    { id: "e003", title: "insurance", amount: 10.9, createdAt: new Date("Jun 18, 2022") },
    { id: "e004", title: "planting", amount: 22.9, createdAt: new Date("Jan 4, 2019") }
]

const Expenses = () => {

    const [expenses, setExpenses] = useState(INITIAL_EXPENSES)

    const [show, setShow] = useState(false);
    const [selYear, setSelYear] = useState('2019')

    const showClickHandler = () => setShow(!show);

    const addNewExpense = expense => {
        setExpenses((prevState) => [expense, ...prevState]);
        setShow(false)
    }

    const closeForm = () => setShow(false);

    const deleteExpense = id => setExpenses(expenses.filter(exp => exp.id !== id));

    const onYearSelected = (selectedYear) => setSelYear(selectedYear);

    const filteredExpenses = expenses.filter(exp => exp.createdAt.getFullYear().toString() === selYear.toString())

    return (
        <>
            <p className='display-3 text-center'>My Expenses App</p>
            <div className="row">
                <div className="col-4 offset-4">
                    <button className="btn btn-dark btn-block" onClick={showClickHandler} >
                        {show ? "Hide" : "Show"}
                    </button>
                </div>
                <div className='col-4'>
                    <ExpenseFilter onYearSelected={onYearSelected} />
                </div>
            </div>

            <br />
            {show && <AddExpense addNewExpense={addNewExpense} closeForm={closeForm} />}
            <br />

            <div className="row">
                {filteredExpenses.map(expense => {
                    return <ExpenseItem
                        key={expense.id}
                        id={expense.id}
                        title={expense.title}
                        amount={expense.amount}
                        createdAt={expense.createdAt}
                        deleteExpense={deleteExpense} />
                })}
            </div>
        </>
    )

}

export default Expenses;


