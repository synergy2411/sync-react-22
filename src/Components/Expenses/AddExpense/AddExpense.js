import { useState } from "react";
import { v4 } from 'uuid';

const AddExpense = (props) => {

    const [enteredTitle, setEnteredTitle] = useState('');
    const [enteredAmount, setEnteredAmount] = useState('')
    const [enteredCreatedAt, setEnteredCreatedAt] = useState('')

    const titleChangeHandler = (event) => setEnteredTitle(event.target.value);
    const amountChangeHandler = event => setEnteredAmount(event.target.value);
    const createdAtChangeHandler = event => setEnteredCreatedAt(event.target.value);

    const submitHandler = (event) => {
        event.preventDefault();
        let expense = {
            id: v4(),
            title: enteredTitle,
            amount: Number(enteredAmount),
            createdAt: new Date(enteredCreatedAt)
        }
        props.addNewExpense(expense);
    }

    return (
        <div className="row">
            <div className="col-6 offset-3">
                <div className="card">
                    <div className="card-body">
                        <p className="text-center display-4">Add Expenses</p>
                        <form onSubmit={submitHandler}>
                            {/* title */}
                            <div className="form-group">
                                <label htmlFor="title">Title : </label>
                                <input type="text"
                                    className="form-control"
                                    onChange={titleChangeHandler}
                                    value={enteredTitle} />

                            </div>
                            {/* amount */}
                            <div className="form-group">
                                <label htmlFor="amount">Amount : </label>
                                <input type="number"
                                    min="0" step="0.1"
                                    className="form-control"
                                    onChange={amountChangeHandler}
                                    value={enteredAmount} />
                            </div>
                            {/* createdAt */}
                            <div className="form-group">
                                <label htmlFor="created-at">Created At : </label>
                                <input type="date"
                                    className="form-control"
                                    min="2019-01-01"
                                    max="2022-04-01"
                                    onChange={createdAtChangeHandler}
                                    value={enteredCreatedAt} />
                            </div>
                            {/* buttons */}
                            <div className="form-group">
                                <div className="row">
                                    <div className="col-6">
                                        <button type="submit"
                                            className="btn btn-warning btn-block">Add</button>
                                    </div>
                                    <div className="col-6">
                                        <button type="button" onClick={() => props.closeForm()}
                                            className="btn btn-light btn-block">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default AddExpense;