import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from '../../context/auth-context';

const Header = () => {
    const ctx = useContext(AuthContext);

    return (
        <header>
            <nav>
                <ul className='nav nav-tabs'>
                    {!ctx.isLoggedIn && <li className='nav-item'>
                        <Link className='nav-link' to="/login">Login</Link>
                    </li>}
                    <li className='nav-item'>
                        <Link className='nav-link' to="/expenses">Expenses</Link>
                    </li>
                    <li className='nav-item'>
                        <Link className='nav-link' to="/courses">Courses</Link>
                    </li>
                    <li className='nav-item'>
                        <Link className='nav-link' to="/counter">Counter</Link>
                    </li>
                    {ctx.isLoggedIn && <li className='nav-item'>
                        <Link className='nav-link' to="/authors">Authors</Link>
                    </li>}
                </ul>
            </nav>
        </header>
    );
}

export default Header;
