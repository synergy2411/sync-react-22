import React from 'react';

const DemoOutput = () => {
    return (
        <div>
            <p>The child component</p>
        </div>
    );
}

export default DemoOutput;
