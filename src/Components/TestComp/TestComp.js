import React, { useEffect, useState } from 'react';
import DemoOutput from './DemoOutput/DemoOutput';

const TestComp = () => {

    const [show, setShow] = useState(true)
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/posts")
            .then(response => response.json())
            .then(result => setPosts(result))
            .catch(console.log)
    }, [])

    return (
        <div>
            <h1>The test component</h1>
            {show && <p>Some cool content here</p>}
            <button onClick={() => setShow(!show)}>Toggle</button>
            <hr />
            <DemoOutput />
            <ul>
                {posts.map(post => <li key={post.id}>{post.title}</li>)}
            </ul>
        </div>
    );
}

export default TestComp;
