import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import TestComp from './TestComp';


test("renders the list of Post title", async () => {
    render(<TestComp />)
    const listItems = await screen.findAllByRole("listitem")
    expect(listItems).not.toHaveLength(0)
})

test("renders the child component", () => {
    render(<TestComp />)
    const childEl = screen.getByText(/child component/i, { exact: false })
    expect(childEl).toBeInTheDocument()
})

test("should not render paragraph when the button is clicked", () => {
    // Arrange
    render(<TestComp />)
    const btnEl = screen.getByText("Toggle")
    // Act
    userEvent.click(btnEl);
    const paragraphEl = screen.queryByText(/some cool content/i, { exact: false })
    expect(paragraphEl).toBeNull();
})

test("renders the paragraph text initially", () => {
    render(<TestComp />)
    const paragraphEl = screen.getByText(/some cool content/i, { exact: false })
    expect(paragraphEl).toBeInTheDocument();
})


test("should not render the text", () => {
    render(<TestComp />);
    const el = screen.queryByText(/the test/i, { exact: false })
    expect(el).not.toBeNull()
})

test("renders the heading", () => {
    // Arrange
    render(<TestComp />);
    const headingEl = screen.getByText("The test component")
    // Act

    // Assert
    expect(headingEl).toBeInTheDocument()
})