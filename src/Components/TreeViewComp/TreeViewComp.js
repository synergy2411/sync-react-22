

import TreeView from '@mui/lab/TreeView'
import TreeItem from '@mui/lab/TreeItem'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
function TreeViewComp() {
    return (
        <div className="App">
            <div className="head" style={{
                width: 'fit-content',
                margin: 'auto',
            }} >
                <h1 style={{ color: 'green' }}>
                    Tree View Component Demo
                </h1>

            </div>

            <TreeView
                aria-label="Tutorials navigator"
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpandIcon={<ChevronRightIcon />}
                sx={{
                    margin: 'auto',
                    flexGrow: 1,
                    width: 'fit-content',
                }}
            >
                <TreeItem nodeId="1" label="Data Structures">
                    <TreeItem nodeId="2" label="Array" onClick={() => alert("Hello")} />
                    <TreeItem nodeId="3" label="Max Heap" />
                    <TreeItem nodeId="4" label="Stack" />
                </TreeItem>
                <TreeItem nodeId="5" label="Algorithms">
                    <TreeItem nodeId="10" label="Gready" />
                    <TreeItem nodeId="6" label="Graph">
                        <TreeItem nodeId="8" label="DFS" />
                        <TreeItem nodeId="8" label="BFS" />
                    </TreeItem>
                </TreeItem>
            </TreeView>
        </div>
    )
}


export default TreeViewComp;