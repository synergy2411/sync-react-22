# What is React?

- JS Library
- Uses Virtual DOM
- Composable
- Declarative
- Create Dynamic App / SPA (react-router-dom)
- Follows one way data binding (Redux -> State Management)
- Component based architecture
- Reconceliation
- Huge developer community
- Open Source
- Lower bundle size - Webpack
- Front-end and backend support (SSR)
- Mobile App Development - (React Native / Ionic Framework)
- Can be plugged in to existing project
- JSX writing
- Ease of use
- Highly interactive
- Light-weight - 30kb
- Maintained and sponsured by Facebook team
- Progressive Web-app (PWA)
- Lazy loading / on-demand loading
- Rendering the UI quickly and efficiently

# Libraries / Framework

- React : Rendering the UI quickly and efficiently (VDOM & Diffing Algorithm)
- \*Angular : Form validation, State Management, Two way data binding, XHR, SPA, template driven etc
- Vue : 'Evan You'. template driven architechture
- \*Next : React Apps for Server Side Rendering
- Svetle : emerging libraray; generate renderable code at compile time only
- LWC : Web Components
- Knockout : 2 way data binding; MVVM pattern
- Backbone : 2 way data binding; handling side effects.
- JQuery : DOM manipulation; Animation; AJAX Calls
- \*Ember : latest version is great.
- Express : NodeJS Framework
- Nest : NodeJS Framework

# Atomic Design Principles in Web Designing / Development

- Atom : Smallest Unit. eg. button, input field, span, p etc
- Molecule : Comb of Atom. eg. Search bar -> input field + button
- Organism : Comb of Molecules. eg. Navigation Bar -> Nav Items, BrandName, Searchbar
- Template : Comb of Organism. eg. Form
- Page : comb of template. eg. A complete web page

# To generate boilerplate code for React

> npx create-react-app my-app

## JSX

- Must have only one Parent element
- Can be embedded with JavaScript and vice-versa
- Creates React "Elements"

## React maintain watchers on STATE variable, not on JavaScript Variables

## Only STATE variables re-render the component

## Controlled Component

- React will maintain the state of the element - input element

## When new State depends upon the previous state, always use setState(function(){})

## Lifting the state up :

- Sending the values from child to parent using functional props

# React - rendering the UI efficiently

## useEffect(callback) - to run any side-effect code

- XHR Call
- Timer
- Mutation / Subscription
- Async Code

## useEffect Flavours-

- useEffect(cb) : call the callback function, every time whenever the component renders.
- useEffect(cb, []) | componentDidMount : callback will execute only once, at the time of initial rendering.
- useEffect(cb, [Deps]) | componentDidUpdate : callback will execute at initial rendering, and whenever the mentioned dependency changes.
- useEffect(cb => cleanUpFn, [Deps]) | componentWillUnmount
  > callback will execute at initial rendering, and whenever the mentioned dependency changes.
  > cleanUp Fn will execute just before the cb fn after initial rendering

1. cb execute
2. dependency change
3. comp will re-render
4. cleanup fn execute
5. cb execute
6. For any sub-sequent re-rendering -> step 3 - step 5
7. Unmounting the comp : cleanUp will execute

## useReducer ->

- Manage more complex state logic
- One state is depending upon another state
- Manage more than one state slice in reducer

## You need to use useCallback() with React.memo() for referential equality checking

## Class based component -> shouldComponentUpdate(nextProps, nextState) / PureComponent

## const memoizedFn = useCallback(cb,[])

## const memoizedValue = useMemo(()=>{return () => {}}, [])

## useCallback(cb, []) === useMemo(()=>()=>{}, [])

# React Routing - Creating SPA

- npm install react-router-dom@6

## npm install json-server -g

## json-server --watch my-db.json --port=9000

# react-router-dom - v6

- BrowserRouter -> history API
- Routes : container for various route configuration
- Route : defines the route config
- Link : creates anchor elements which does not reload the page
- Nested Routes / Child Routes
- useParams() : access of all route parameters => {...}
- useNavigate("/path") : to programmatically navigate the use
- Query Parameters/ Query String - URLSearchParams (mdn) => access of search / query parameters

Comp A -> Comp B

Comp B

useEffect( () => {
return () => {

    <!-- unmount the same / child comp -->

    props.onCloseChildElement()

} // CLEAN UP
})
