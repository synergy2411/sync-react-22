{
"authors": [
{ "id": "a001", "name": "john", "email": "john@test" },
{ "id": "a002", "name": "alice", "email": "alice@test" },
{ "id": "a003", "name": "jenny", "email": "jenny@test" }
],
"books": [
{
"id": "b001",
"title": "Learn React in 7 days",
"numOfPages": 199,
"authorId": "a003"
},
{
"id": "b002",
"title": "Awesome React",
"numOfPages": 249,
"authorId": "a002"
},
{
"id": "b003",
"title": "Mastering Node",
"numOfPages": 359,
"authorId": "a001"
},
{
"id": "b004",
"title": "Angular 101",
"numOfPages": 400,
"authorId": "a002"
}
]
}
