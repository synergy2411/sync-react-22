const redux = require("redux");

const initialState = {
    todos: []
}

// Reducer
function rootReducer(state = initialState, action) {
    if (action.type === "ADD_TODO") {
        return {
            todos: [action.label, ...state.todos]
        }
    } else if (action.type === "DELETE_TODO") {
        const duplicateTodos = state.todos.filter((todo, indx) => indx !== action.index)
        return {
            todos: duplicateTodos
        }
    }
    return state;
}

const store = redux.createStore(rootReducer)

// Subscription
store.subscribe(() => {
    console.log("STATE : ", store.getState());
})

// console.log("STATE : ", store.getState());

// Action - What happened in App
store.dispatch({ type: "ADD_TODO", label: "to renew car insurance" })

// console.log("STATE AFTER ADD TODO 01: ", store.getState());

store.dispatch({ type: "ADD_TODO", label: "to pot the plant" })

// console.log("STATE AFTER ADD TODO 02: ", store.getState());

store.dispatch({ type: "DELETE_TODO", index: 1 })