class Student {

    static numOfStudent = 0;
    // static
    static getNumOfStudent() {
        return Student.numOfStudent;
    }
    constructor() {
        Student.numOfStudent++;
    }
    // via instance
    getDetails() {
    }

}

let foo = new Student();
let bar = new Student();
let bam = new Student();

// foo.getNumOfStudent()

Student.getNumOfStudent();